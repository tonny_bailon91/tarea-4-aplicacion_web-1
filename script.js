document.addEventListener("DOMContentLoaded", function () {
    const numero1Input = document.getElementById("numero1");
    const numero2Input = document.getElementById("numero2");
    const resultadoDiv = document.getElementById("resultado");

    function realizarOperacion(operacion) {
        const num1 = parseFloat(numero1Input.value);
        const num2 = parseFloat(numero2Input.value);

        if (!isNaN(num1) && !isNaN(num2)) {
            switch (operacion) {
                case "suma":
                    resultadoDiv.textContent = `Resultado: ${num1 + num2}`;
                    break;
                case "resta":
                    resultadoDiv.textContent = `Resultado: ${num1 - num2}`;
                    break;
                case "multiplicacion":
                    resultadoDiv.textContent = `Resultado: ${num1 * num2}`;
                    break;
                case "division":
                    if (num2 !== 0) {
                        resultadoDiv.textContent = `Resultado: ${num1 / num2}`;
                    } else {
                        resultadoDiv.textContent = "No se puede dividir por 0";
                    }
                    break;
                case "modulo":
                    if (num2 !== 0) {
                        resultadoDiv.textContent = `Resultado: ${num1 % num2}`;
                    } else {
                        resultadoDiv.textContent = "No se puede calcular el módulo por 0";
                    }
                    break;
                default:
                    resultadoDiv.textContent = "Seleccione una operación válida";
            }
        } else {
            resultadoDiv.textContent = "Ingrese números válidos en ambos campos";
        }
    }

    document.getElementById("suma").addEventListener("click", () => {
        realizarOperacion("suma");
    });

    document.getElementById("resta").addEventListener("click", () => {
        realizarOperacion("resta");
    });

    document.getElementById("multiplicacion").addEventListener("click", () => {
        realizarOperacion("multiplicacion");
    });

    document.getElementById("division").addEventListener("click", () => {
        realizarOperacion("division");
    });

    document.getElementById("modulo").addEventListener("click", () => {
        realizarOperacion("modulo");
    });
});